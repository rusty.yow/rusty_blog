import * as React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql, Link } from "gatsby"
import styled, { ThemeProvider} from "styled-components"
import { theme } from "../utils/theme"
import Header from "./header"
import "./layout.css"
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar, Nav } from "react-bootstrap"

const PageContainer = styled.div`
  
  flex-direction: column;
  min-height: 100vh;
`

const ChildContainer = styled.div`
  flex-grow: 1;
  padding-left: 29px;
  padding-right: 29px;
  max-width: 1000px;
  margin: auto;
`

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    
<ThemeProvider theme={theme}>
      <PageContainer>
        <Header siteTitle={data.site.siteMetadata?.title || `Title`} />
         <ChildContainer>{children}</ChildContainer>
         <div class="footer">
            <footer
              style={{
                marginTop: `2rem`,            
              }}
            >
            
              © {new Date().getFullYear()}, Design By: Rusty Yow, built with 
              {` `}
              <a href="https://www.gatsbyjs.com">Gatsby</a>
            
            </footer>
         </div> 
      </PageContainer>

</ThemeProvider>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
