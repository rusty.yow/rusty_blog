import * as React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import { Navbar, Nav } from "react-bootstrap"


const Header = ({ siteTitle }) => (
  <header
    style={{
      background: `#DFF0F9`,
    }}
  >
    
    <div class="header">
        <Navbar expand="md">
          <Navbar.Brand href="/">{siteTitle}</Navbar.Brand>
            <Navbar.Toggle aria-controls="navbarResponsive"/>
              <Navbar.Collapse id="navbarResponsive">
                <Nav className="text-center ml-auto">
                  <ul className="navbar-nav">
                    <li>
                      <Link to="/" className="nav-link" activeClassName="active">Home</Link>
                    </li>
                    <li>
                      <Link to="/page-2/" className="nav-link" activeClassName="active">Notes</Link>
                    </li>
                    <li>
                      <Link to="/about/" className="nav-link" activeClassName="active">About</Link>
                    </li>
                    <li>
                      <Link to="/contact/" className="nav-link" activeClassName="active">Contact</Link>
                    </li>
                  </ul>
                </Nav>
              </Navbar.Collapse>
        </Navbar>
        
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
