import * as React from "react"
import { Link, useStaticQuery, graphql } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
//import Image from "../components/images"
import Img from "gatsby-image"
import 'bootstrap/dist/css/bootstrap.min.css';

const IndexPage = () => {

  const data = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "profilepic.png" }) {
        childImageSharp {
          fixed(width: 300, height: 300) {
            ...GatsbyImageSharpFixed
          }
        }
      }

      allMdx(
        sort: { fields: [frontmatter___date], order: DESC }
        filter: { frontmatter: { published: { eq: true }, post: { eq: "blog" } } }
      ) {
        nodes {
          id
          excerpt(pruneLength: 250)
          frontmatter {
            title
            author
            slug
            description
            date
          }
        }
      }
    }
  `)

  return (
    
    <Layout>
      <SEO title="Home" />
      
      <div class="intro-wrapper">
        
        <div class="gridLeft">
          <Img fixed={data.file.childImageSharp.fixed}
                  alt="Rusty Yow Profile Picture"
                  className="rounded-circle"
                />
        </div>

        <div class="gridRight">
            <h2>Hey i'm Rusty. I'm a System Administrator for the University of Texas and this is my blog!</h2>
        </div>

      </div>

        <div class="homeContent">
          <p class="latestPosts">Latest Posts</p>
          {data.allMdx.nodes.map(({ post, excerpt, frontmatter }) => (
            <>
            

            <li class="list-group list-group-flush py-0 px-0 mb-0">
              <Link to={frontmatter.slug} class="list-group-item list-group-item-action py-1 px-1 mb-1"><p class="latestPostsDescription"><b>{frontmatter.title}</b></p>
                <p class="latestPostsDate">{frontmatter.date}</p>
                <p class="latestPostsDescription">{frontmatter.description}</p>
              </Link>
          </li>
           
           <hr class="latestPostsHR"></hr>

            </>
          ))}
        </div>
      
    </Layout>
  )
}

export default IndexPage
