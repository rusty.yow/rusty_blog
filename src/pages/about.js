import React from 'react'
import Layout from '../components/layout'
import SEO from "../components/seo"

const about = () => {
    return (
        <Layout>
        <SEO title="About" />    
            <h1>About Page</h1>
            <p>Nothing here yet...</p>
        </Layout>
    )
}

export default about
