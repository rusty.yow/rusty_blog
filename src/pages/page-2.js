import * as React from "react"
import { Link, useStaticQuery, graphql } from "gatsby"
import 'bootstrap/dist/css/bootstrap.min.css';
import Layout from "../components/layout"

const SecondPage = () => {

  const data = useStaticQuery(graphql`
  query {
    file(relativePath: { eq: "profilepic.png" }) {
      childImageSharp {
        fixed(width: 300, height: 300) {
          ...GatsbyImageSharpFixed
        }
      }
    }

    allMdx(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { published: { eq: true }, post: { eq: "other" } } }
    ) {
      nodes {
        id
        frontmatter {
          title
          author
          slug
          description
          date
          published
        }
      }
    }
  }
`)



  
return (
  
  <Layout>
    <div class="homeContent">
      <h1>Other Posts</h1>
      {data.allMdx.nodes.map(({ post, excerpt, frontmatter }) => (
            <>
              <Link to={`/${frontmatter.slug}`}><p class="latestPostsDescription"><b>{frontmatter.title}</b></p></Link>
              <p class="latestPostsDate">{frontmatter.date}</p>
              <p class="latestPostsDescription">{frontmatter.description}</p>
            </>
          ))}
    </div>
  </Layout>

  )
  }


export default SecondPage
