import React from 'react'
import Layout from '../components/layout'
import SEO from "../components/seo"

const contact = () => {
    return (
        <Layout>
        <SEO title="Contact" />    
            <h1>Contact Page</h1>
            <p>Nothing here yet...</p>
        </Layout>
    )
}

export default contact
